const { app, server, io } = require("./app.js");
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const session = require('express-session');
const indexRouter = require('./routes/index');
const { onConnection, onDisconnection } = require('./sockets/events.js');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(session({ secret: 'supersecret' }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);

io.on('connection', socket => {
    socket.on('save_session', uid => {
        onConnection(uid, socket);
        socket.once('disconnect', onDisconnection.bind(null, uid));
        socket.once('error', onDisconnection.bind(null, uid));
    });
});

server.listen(3000, (req, res) => {
    console.log('Server started on port 3000');
});

require('./cron/getBtcPrice.js');