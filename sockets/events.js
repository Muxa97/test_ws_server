const store = require('../store.js');

const onConnection = (uid, socket) => {
    store.addSID(uid, socket.id);
};

const onDisconnection = (uid) => {
    store.removeSID(uid);
};

module.exports = {
    onConnection,
    onDisconnection
};