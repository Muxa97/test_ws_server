

class LocalMemory {
    constructor() {
        this.BTC_RATE = 0;
        this.SESSIONS = new Map();
    }

    setRate(rate) {
        if (rate && !isNaN(+rate)) {
            this.BTC_RATE = rate;
        }
    }

    getRate() {
        return this.BTC_RATE;
    }

    addSID(id, sid) {
        this.SESSIONS.set(id, sid);
    }

    getSID(id) {
        return this.SESSIONS.get(id);
    }

    removeSID(id) {
        this.SESSIONS.delete(id);
    }
}

const store = new LocalMemory();

module.exports = store;