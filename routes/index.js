const express = require('express');
const router = express.Router();
const store = require('../store.js');
const { io } = require('../app.js');

/* GET home page. */
router.get('/', (req, res) => {
  res.render('index', { title: 'Express' });
});

router.post('/get_rate', (req, res) => {
  try {
    const { id } = req.body;
    if (!id) {
      throw new Error('Invalid request body');
    }

    const sid = store.getSID(id);
    if (!sid) {
      throw new Error('Session doesn\'t exist');
    }

    io.sockets.to(sid).emit('btc_rate', { rate: store.getRate() });
    res.end();
  } catch (err) {
    console.log('Error POST /get_rate:', err);
  }
});

module.exports = router;
