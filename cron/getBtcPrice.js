const fetch = require('node-fetch');
const config = require('../config.js');
const store = require('../store.js');

async function fetchBtcRate() {
    const response = await fetch(`${config.BTC_PRICE_ENDPOINT}&api_key=${config.CRYPTO_COMPARE_API_KEY}`)
        .then(res => res.json());
    return response.USD;
};

module.exports = (async () => {
    fetchBtcRate().then(res => store.setRate(res));
    setInterval(async () => {
        store.setRate(await fetchBtcRate());
    }, 60000)
})();