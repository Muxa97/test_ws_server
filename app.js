const express = require('express');
const http = require('http');
const wss = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = new wss(server);

module.exports = { app, server, io };